from fastapi import APIRouter

from api.api_v1.endpoints import hello, inquery, addestramento

api_router = APIRouter()
api_router.include_router(hello.router, prefix="/hello", tags=["hello"])
api_router.include_router(inquery.router, prefix="/inquery", tags=["inquery"])
api_router.include_router(addestramento.router, prefix="/addestramento", tags=["addestramento"])

