from sqlalchemy import Column, Integer, String
from db.database import Base


class Phrases(Base):
    """
    Phrases model
    """

    __tablename__ = "phrases"

    id = Column(Integer, primary_key=True, index=True)
    phrases = Column(String, unique=True, index=True, nullable=False)
    tag = Column(String, index=True, nullable=False)
