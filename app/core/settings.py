from typing import Optional, Dict, Any
from urllib.parse import quote_plus
from pydantic import BaseSettings, PostgresDsn, validator


class Settings(BaseSettings):
    """
    Class Settings
    """

    PROJECT_NAME: str = "Sentiment-Analysis"
    DB_USER: str
    DB_PASSWORD: str
    DB_NAME: str
    POSTGRES_SERVER: str
    POSTGRES_PORT: str
    SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        """
        Return db value parameter
        """
        if isinstance(v, str):
            return v
        return PostgresDsn.build(
            scheme="postgresql",
            user=Settings.get_url_encoded(values.get("DB_USER")),
            password=Settings.get_url_encoded(values.get("DB_PASSWORD")),
            host=values.get("POSTGRES_SERVER"),
            port=values.get("POSTGRES_PORT"),
            path=f"/{values.get('DB_NAME') or ''}",
        )

    @classmethod
    def get_url_encoded(cls, url: str) -> str:
        """
        Return url encoded
        """
        return quote_plus(url)

    class Config:
        """
        Provide configuration to Pydantic
        """

        case_sensitive = True
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
