from typing import List
from sqlalchemy.orm import Session
from models.phrases import Phrases
from schemas.phrase import PhraseCreate, PhraseUpdate
from crud.base import CRUDBase


class CRUDPhrase(CRUDBase[Phrases]):
    """
    CRUDPhrase class
    """

    def get_phrase(self, db: Session, phrase_id: int) -> Phrases:
        """
        Get phrase by id
        """
        return db.query(Phrases).filter(Phrases.id == phrase_id).first()

    def get_phrases(self, db: Session, skip: int = 0, limit: int = 100) -> List[Phrases]:
        """
        Get list of phrases
        """
        return db.query(Phrases).offset(skip).limit(limit).all()

    def get_phrase_by_phrase(self, db: Session, phrase: str) -> Phrases:
        """
        Get phrase by phrase
        """
        return db.query(Phrases).filter( Phrases.phrases == phrase).first()

    def create_phrase(self, db: Session, phrase: PhraseCreate) -> Phrases:
        """
        Create user
        """
        db_user = Phrases(
            phrases=phrase.phrase,
            tag=phrase.tag,
        )
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user

    def remove_phrase(self, db: Session, phrases_id: int):
        """
        Delete phrase
        """
        db_phrase = self.get_phrase(db, phrases_id)
        db.delete(db_phrase)
        db.commit()

    def update_phrase(self, db: Session, user: PhraseUpdate, phrases_id: int) -> Phrases:
        """
        Update user information
        """
        db_user = self.get_phrase(db, phrases_id)
        for field, value in user.__dict__.items():
            setattr(db_user, field, value)
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user



crud_phrases = CRUDPhrase(Phrases)
