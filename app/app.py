import uvicorn
from fastapi import FastAPI
from api.api_v1.apis import api_router
from core.settings import settings
from db.database import SessionLocal
from db.init_db import init_db

app = FastAPI(title=settings.PROJECT_NAME)
app.include_router(api_router)

if __name__ == "__main__":
    db = SessionLocal()
    init_db(db)

    uvicorn.run("app:app", port=8080, log_level="debug")
