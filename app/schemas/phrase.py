from typing import Optional
from pydantic import BaseModel


class PhraseBase(BaseModel):
    """
    Default phrase properties
    """

    phrase: Optional[str] = None
    tag: Optional[str] = None


class PhraseCreate(PhraseBase):
    """
    Properties for create phrase via Api
    """

    phrase: str


class PhraseInDBBase(PhraseBase):
    """
    phrase class
    """

    id: Optional[int] = None

    class Config:
        orm_mode = True


class PhraseUpdate(PhraseBase):
    """
    Properties updates
    """
    id: int
    tag: str


class Phrase(PhraseInDBBase):
    """
    Additional properties to return to client
    """


class PhraseInDB(PhraseInDBBase):
    """
    Additional properties stored in DB
    """

    tag: str


class PhraseToken(Phrase):
    """
    Additional parameters for OAuth2
    """

    tag: str
